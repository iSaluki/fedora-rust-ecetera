### Etcetera

[![Copr build status](https://copr.fedorainfracloud.org/coprs/saluki/rust-etcetera/package/rust-etcetera/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/saluki/rust-etcetera/package/rust-etcetera/)

- This repository is used for packaging the [Etcetera](https://crates.io/crates/etcetera) rust crate for Fedora.
- Please report issues you experience via the RedHat Bugzilla if it is with an installed version of the package, if you have an issue with the source included here, open an issue in this repository.

#### COPR

- This package is also on COPR. You can find it [here](https://copr.fedorainfracloud.org/coprs/saluki/rust-etcetera/)
- COPR will be removed in favour of official repos in future, once I have completed necessary sponsorships.